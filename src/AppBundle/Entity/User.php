<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="membres")
 * @UniqueEntity(fields={"login"}, errorPath="login")
 * @UniqueEntity(fields={"mail"}, errorPath="mail")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="idMembre", type="integer", length=11)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idMembre;

    public function getIdMembre()
    {
        return $this->idMembre;
    }

    public function setIdMembre($idMembre)
    {
        $this->idMembre = $idMembre;
    }


    /**
     * @ORM\Column(unique=true, type="string", length=32)
     * @Assert\NotNull()
     * @Assert\Length(min=6, max=20)
     */
    private $login;

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }


    /**
     * @Assert\NotNull()
     * @Assert\Length(min=5, max=20)
     */
    private $rawPassword;

    public function getRawPassword()
    {
        return $this->rawPassword;
    }

    public function setRawPassword($rawPassword)
    {
        $this->rawPassword = $rawPassword;
    }

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $password;

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotNull()
     * @Assert\Length(min=3, max=25)
     */
    private $nom;

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }


    /**
     * @ORM\Column(unique=true, type="string", length=50)
     * @Assert\NotNull()
     * @Assert\Length(min=3, max=30)
     */
    private $prenom;

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @ORM\Column(unique=true, type="string", length=250)
     * @Assert\Email(checkHost=true)
     */
    private $mail;

    public function getMail()
    {
        return $this->mail;
    }

    public function setMail($mail)
    {
        $this->mail = $mail;
    }


    /**
     * @ORM\Column(type="string", length=20)
     */
    private $statut;

    public function getStatut()
    {
        return $this->statut;
    }

    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @ORM\Column(name="dateCrea", type="date")
     */
    private $dateCrea;

    public function getDateCrea()
    {
        return $this->dateCrea;
    }

    public function setDateCrea($dateCrea)
    {
        $this->dateCrea = $dateCrea;
    }


    /**
     * @ORM\Column(type="string", length=100)
     */
    private $gravatar;

    public function getGravatar()
    {
        return $this->gravatar;
    }

    public function setGravatar($gravatar)
    {
        $this->gravatar = $gravatar;
    }


    public function getRoles()
    {
        $roles = ['ROLE_USER'];

        if ($this->getStatut() === "admin") {
            $roles[] = 'ROLE_ADMIN';
        }

        return $roles;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->login;
    }

    public function eraseCredentials()
    {
        $this->rawPassword = null;
    }


    public function __construct()
    {
        $this->statut = "membre";
        $this->dateCrea = new \DateTime();
        $this->recettes = new ArrayCollection();
        $this->gravatar = "unknown.jpg";
    }


}