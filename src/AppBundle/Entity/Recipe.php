<?php

namespace AppBundle\Entity;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="recettes")
 */
class Recipe extends AbstractType
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", name="idRecette", length=11)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idRecette;

    public function getIdRecette()
    {
        return $this->idRecette;
    }

    public function setIdRecette($idRecette)
    {
        $this->idRecette = $idRecette;
    }

    /**
     * @ORM\Column(type="date", name="dateCrea")
     */
    private $dateCrea;

    public function getDateCrea()
    {
        return $this->dateCrea;
    }

    public function setDate($date)
    {
        $this->dateCrea = $date;
    }

    /**
     * @ORM\Column(type="string", length=250)
     * @Assert\NotNull()
     * @Assert\Length(min=5, max=30)
     */
    private $titre;


    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=30, max=600)
     */
    private $chapo;


    /**
     * @ORM\Column(name="tempsPreparation", type="integer", length=11)
     */
    private $tempsPreparation;

    /**
     * @ORM\Column(name="tempsCuisson", type="integer", length=11)
     */
    private $tempsCuisson;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $parts;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $difficulte;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=3, max=30)
     */
    private $ingredient;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=20, max=500)
     */
    private $preparation;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $img;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $couleur;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $membre;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $categorie;


    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getChapo()
    {
        return $this->chapo;
    }

    /**
     * @param mixed $chapo
     */
    public function setChapo($chapo)
    {
        $this->chapo = $chapo;
    }

    /**
     * @return mixed
     */
    public function getTempsPreparation()
    {
        return $this->tempsPreparation;
    }

    /**
     * @param mixed $tempsPreparation
     */
    public function setTempsPreparation($tempsPreparation)
    {
        $this->tempsPreparation = $tempsPreparation;
    }

    /**
     * @return mixed
     */
    public function getTempsCuisson()
    {
        return $this->tempsCuisson;
    }

    /**
     * @param mixed $tempsCuisson
     */
    public function setTempsCuisson($tempsCuisson)
    {
        $this->tempsCuisson = $tempsCuisson;
    }

    /**
     * @return mixed
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * @param mixed $parts
     */
    public function setParts($parts)
    {
        $this->parts = $parts;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return mixed
     */
    public function getDifficulte()
    {
        return $this->difficulte;
    }

    /**
     * @param mixed $difficulte
     */
    public function setDifficulte($difficulte)
    {
        $this->difficulte = $difficulte;
    }

    /**
     * @return mixed
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * @param mixed $ingredient
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;
    }

    /**
     * @return mixed
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * @param mixed $preparation
     */
    public function setPreparation($preparation)
    {
        $this->preparation = $preparation;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * @param mixed $couleur
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    }

    /**
     * @return mixed
     */
    public function getMembre()
    {
        return $this->membre;
    }

    /**
     * @param mixed $membre
     */
    public function setMembre($membre)
    {
        $this->membre = $membre;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }


}