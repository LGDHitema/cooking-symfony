<?php
/**
 * Created by PhpStorm.
 * User: MUSH
 * Date: 16/02/2019
 * Time: 11:17
 */

namespace AppBundle\Controller;


use AppBundle\Form\SignupType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexionAction(Request $request)
    {
        return $this->render('login/connexion.html.twig');
    }

    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscriptionAction(Request $request, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $user = new User();

        $form = $this->createForm(SignupType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $userPasswordEncoder->encodePassword($user, $user->getRawPassword());
            $user->eraseCredentials();
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();


            $this->addFlash('success', 'Vous avez bien été enregistré.e !');
            return $this->redirectToRoute('index');
        }

        return $this->render('user/inscription.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profil", name="profil")
     */
    public function profilAction()
    {
        $user = $this->getUser();
        return $this->render('user/profil.html.twig', [
            'user' => $user,
        ]);

        /*$this->addFlash('success', 'Vos informations ont bien été modifées.');
        return $this->redirectToRoute('profil');*/
    }
}