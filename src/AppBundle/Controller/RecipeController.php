<?php
/**
 * Created by PhpStorm.
 * User: MUSH
 * Date: 16/02/2019
 * Time: 11:17
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Recipe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RecipeController
 * @package AppBundle\Controller
 */
class RecipeController extends Controller
{
    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request)
    {
        dump($request->query->get('option'));
        return $this->render('recipes/search.html.twig');
    }

    /**
     * @Route("/recipe/{idRecette}", name="recipe")
     */
    public function recipeAction(Recipe $recipe)
    {
        return $this->render('recipes/recipe.html.twig', [
            'recipe' => $recipe,
        ]);
    }
}