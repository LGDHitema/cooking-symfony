<?php
/**
 * Created by PhpStorm.
 * User: MUSH
 * Date: 18/02/2019
 * Time: 14:55
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class SecurityController extends Controller
{
    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexionAction(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $username = $authenticationUtils->getLastUsername();

        return $this->render('login/connexion.html.twig', [
            'error' => $error,
            'username' => $username,
        ]);
    }

    /**
     * @Route("/hello", name="hello")
     * @Security("has_role('ROLE_USER')")
     */
    public function helloAction()
    {
        $user = $this->getUser();
        $this->addFlash('success', 'Vous vous êtes connecté avec succès. Bienvenue ' . $user->getLogin() . ' !');

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/sayonara", name="sayonara")
     * @Security("has_role('ROLE_USER')")
     */
    public function goodbyeAction()
    {
        $this->addFlash('warning', 'Vous vous êtes déconnecté avec succès. Au revoir !');

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/redirect", name="redirect")
     */
    public function redirectAction()
    {
        $this->addFlash('warning', 'Vous devez être connecté pour accéder à ce contenu !');

        return $this->redirectToRoute('index');
    }
}