<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


class SignupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('login', TextType::class, [
                'label' => 'Votre login:',
            ])
            ->add('nom', TextType::class, [
                'label' => 'Votre nom:',
            ])
            ->add('prenom', TextType::class, [
                'label' => 'Votre prenom:',
            ])
            ->add('rawPassword', PasswordType::class, [
                'label' => 'Votre mot de passe:',
            ])
            ->add('mail', EmailType::class, [
                'label' => 'Votre email:',
            ]);
    }
}